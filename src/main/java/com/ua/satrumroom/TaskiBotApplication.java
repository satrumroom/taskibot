package com.ua.satrumroom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TaskiBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskiBotApplication.class, args);
    }

}
