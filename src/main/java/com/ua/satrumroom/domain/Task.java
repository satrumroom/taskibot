package com.ua.satrumroom.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Task implements Serializable {

    private static final long serialVersionUID = -3317032419407307131L;
    private int taskId;
    private Status status;
    private LocalDateTime createDateTime;
    private LocalDateTime deadline;
    private String description;
    private String userID;

}
