package com.ua.satrumroom.domain;

public enum Status {
    COMPLETED,
    PENDING,
    IN_PROGRESS,
}
