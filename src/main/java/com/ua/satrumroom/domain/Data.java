package com.ua.satrumroom.domain;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@NoArgsConstructor
public class Data {

    private static Data INSTANCE;
    private final Map<String, List<Task>> usersAndTasks = new HashMap<>();

    public void initializeDataMap() {
        Path pathToRootFolder = Path.of("Tasks repository/");

        if (!Files.exists(pathToRootFolder)) {
            try {
                Files.createDirectory(pathToRootFolder);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        long amountOfUsers = getAmountOfUsers(pathToRootFolder);

        if (amountOfUsers != 0) {
            for (int i = 1; i <= amountOfUsers; i++) {
                List<Task> taskList;
                String key = String.valueOf(i);
                String pathToFile = pathToRootFolder.toString() + i + "/user_tasks.bin";

                try (FileInputStream fis = new FileInputStream(pathToFile);
                     ObjectInputStream ois = new ObjectInputStream(fis)) {
                    taskList = (List) ois.readObject();
                    usersAndTasks.put(key, taskList);
                } catch (IOException | ClassNotFoundException ignored) {
                    //TODO in progress!
                }
            }
        }
    }

    protected long getAmountOfUsers(Path pathToRootFolder) {
        long amountOfUsers;

        File file = new File(String.valueOf(pathToRootFolder));
        File[] files = file.listFiles(File::isDirectory);
        assert files != null;
        amountOfUsers = files.length;
        return amountOfUsers;
    }

    public Map<String, List<Task>> getUsersAndTasks() {
        return usersAndTasks;
    }

    public static Data getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Data();
        }
        return INSTANCE;
    }
}