package com.ua.satrumroom.repository;

import com.ua.satrumroom.domain.Data;
import com.ua.satrumroom.domain.Task;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

@Component
public class UserFileRepository implements UserRepository {
    private final Data data = Data.getInstance();


    public void add(String userId) {
        Path mainFolder = Path.of("Task repository/");
        Path usersFolderName = getUsersFolderName(userId);

        addNewUserToDataMap(userId);

        if (!Files.exists(mainFolder)) {
            createNewFolder(mainFolder);
        }

        if (!Files.exists(usersFolderName)) {
            createNewFolder(usersFolderName);
        }
    }


    @Override
    public void get(String userId) {

    }

    @Override
    public void remove(String userId) {
        deleteUsersFolder(userId);
        deleteUserFromDataMap(userId);
    }

    @Override
    public void update(String userId) {
        //TODO in process
    }

    private void createNewFolder(Path mainFolder) {
        try {
            Files.createDirectory(mainFolder);
        } catch (IOException ignored) {
            //TODO in progress
        }
    }

    public Path getUsersFolderName(String userId) {
        return Path.of("Task repository/" + userId);
    }

    private void deleteUsersFolder(String userId) {
        Path usersFolderName = getUsersFolderName(userId);

        if (Files.exists(usersFolderName)) {
            try {
                Files.delete(usersFolderName);
            } catch (IOException ignored) {
                //TODO in progress
            }
        }
    }

    private void deleteUserFromDataMap(String userId) {
        data.getUsersAndTasks().remove(userId);
    }

    private void addNewUserToDataMap(String userId) {
        if (!data.getUsersAndTasks().containsKey(userId)) {
            ArrayList<Task> emptyTasksList = new ArrayList<>();
            data.getUsersAndTasks().put(userId, emptyTasksList);
        }
    }
}