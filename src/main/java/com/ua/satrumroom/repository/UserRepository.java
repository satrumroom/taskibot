package com.ua.satrumroom.repository;

import org.springframework.stereotype.Component;

import java.io.Serializable;


public interface UserRepository {

    void add(String userId);

    void get(String userId);

    void remove(String userId);

    void update(String userId);
}
