package com.ua.satrumroom.repository;

import com.ua.satrumroom.domain.Data;
import com.ua.satrumroom.domain.Task;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class TaskFileRepository implements TaskRepository {
    private final Data data = Data.getInstance();

    @PostConstruct
    public void init() {
        data.initializeDataMap();
    }

    @Override
    public void save(Task task, String userId) {
        List<Task> tasksList = new ArrayList<>();
        boolean hasUserInMap = data.getUsersAndTasks().containsKey(userId);

        data.getUsersAndTasks().remove(userId);

        if (hasUserInMap) {
            try (FileInputStream fis = new FileInputStream(getPathToFile(userId));
                 ObjectInputStream ois = new ObjectInputStream(fis)) {
                List<Task> buf = (List) ois.readObject();
                tasksList = buf;
                task.setTaskId(tasksList.size() + 1);
                tasksList.add(task);
            } catch (IOException | ClassNotFoundException ignored) {
                //TODO in progress
            }

            try (FileOutputStream fos = new FileOutputStream(getPathToFile(userId));
                 ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(tasksList);
                data.getUsersAndTasks().put(userId, tasksList);
            } catch (IOException ignored) {
                //TODO in progress
            }
        } else {
            task.setTaskId(1);
            tasksList.add(task);

            try (FileOutputStream fos = new FileOutputStream(getPathToFile(userId));
                 ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(tasksList);
                data.getUsersAndTasks().put(userId, tasksList);
            } catch (IOException ignored) {
                //TODO in progress
            }
        }
    }

    @Override
    public void findOne(String userId) {

        try (FileInputStream fis = new FileInputStream(getPathToFile(userId));
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            List<Task> buf = (List) ois.readObject();
        } catch (IOException | ClassNotFoundException ignored) {
            //TODO in progress
        }
    }

    @Override
    public void update(Task task, int taskId, String userId) {
        List<Task> taskList = new ArrayList<>();
        data.getUsersAndTasks().remove(userId);

        try (FileInputStream fis = new FileInputStream(getPathToFile(userId));
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            List<Task> buf = (List) ois.readObject();
            taskList = buf;
        } catch (IOException | ClassNotFoundException ignored) {
            //TODO in progress
        }

        if (taskId == 1) {
            taskList.remove(0);
            taskList.add(0, task);
        } else {
            taskList.remove(taskId - 1);
            taskList.add(taskId - 1, task);
        }

        try (FileOutputStream fis = new FileOutputStream(getPathToFile(userId));
             ObjectOutputStream ois = new ObjectOutputStream(fis)) {
            ois.writeObject(taskList);
        } catch (IOException ignored) {
            //TODO in progress
        }
        data.getUsersAndTasks().put(userId, taskList);
    }

    @Override
    public void delete(int taskId, String userId) {
        List<Task> taskList = new ArrayList<>();
        data.getUsersAndTasks().remove(userId);

        try (FileInputStream fis = new FileInputStream(getPathToFile(userId));
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            List<Task> buf = (List) ois.readObject();
            taskList = buf;
        } catch (IOException | ClassNotFoundException ignored) {
            //TODO in progress!
        }

        if (taskId == 1) {
            taskList.remove(0);
        } else {
            taskList.remove(taskId - 1);
        }

        try (FileOutputStream fis = new FileOutputStream(getPathToFile(userId));
             ObjectOutputStream ois = new ObjectOutputStream(fis)) {
            ois.writeObject(taskList);
        } catch (IOException ignored) {
            //TODO in progress
        }
        data.getUsersAndTasks().put(userId, taskList);
    }

    private String getPathToFile(String userId) {
        return "Task repository/" + userId + "/user_tasks.bin";
    }
}