package com.ua.satrumroom.repository;

import com.ua.satrumroom.domain.Task;
import org.springframework.stereotype.Component;

import java.io.Serializable;

public interface TaskRepository {

    void save(Task task, String userId);

    void delete(int taskId, String userId);

    void findOne(String userId);

    void update(Task task, int taskId, String userId);

}