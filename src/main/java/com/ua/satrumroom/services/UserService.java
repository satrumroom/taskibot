package com.ua.satrumroom.services;

public interface UserService {
    void save();
    void findOne(String id);
    void delete(String id);
    void update();

}
