package com.ua.satrumroom.services;

public interface TaskService {
    void save();
    void findOne(int id);
    void delete(int id);
    void update();
}
