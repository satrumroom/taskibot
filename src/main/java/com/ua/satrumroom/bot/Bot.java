package com.ua.satrumroom.bot;

public interface Bot {

    String START_COMMAND = "/start";
    String CREATE_TASK_COMMAND = "/createtask";
    String SHOW_ALL_TASKS_COMMAND = "/showtasks";

    void sendMessage(long chatId, String message);
}
