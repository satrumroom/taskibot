package com.ua.satrumroom.bot.telegram;

import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.pengrad.telegrambot.request.SendMessage;
import com.ua.satrumroom.bot.Bot;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class TelegramInterfaceMaker {

    public SendMessage createMain(Message message) {
        long chatId = message.chat().id();
        InlineKeyboardMarkup replyKeyboardMarkup = new InlineKeyboardMarkup(
                buildInlineButton("Создать задачу\n", Bot.CREATE_TASK_COMMAND),
                buildInlineButton("Мои задачи\n", Bot.SHOW_ALL_TASKS_COMMAND));
        return new SendMessage(chatId, "Привет " + message.chat().firstName() + "!").replyMarkup(replyKeyboardMarkup);
    }

    public SendMessage showAllTasks(CallbackQuery callbackQuery) {
        long chatId = callbackQuery.message().chat().id();
        InlineKeyboardButton[][] strButtonsArray = getInlineKeyboardButtons(
                buildInlineButton("Купить батон", "1"),
                buildInlineButton("Помыть ботинки", "2"),
                buildInlineButton("Починить унитаз", "3"),
                buildInlineButton("Записаться к врачу", "4"),
                buildInlineButton("Погулять с псом", "5")
        );

        InlineKeyboardMarkup replyKeyboardMarkup = new InlineKeyboardMarkup(strButtonsArray);
        return new SendMessage(chatId, "Ваши задачи:").replyMarkup(replyKeyboardMarkup);
    }

    /**
     * Create field of buttons. 2 rows, "buttonsArray/2" columns
     * @param buttonsArray InlineKeyboardButtons
     * @return InlineKeyboardButton[][] (field of buttons)
     */
    private InlineKeyboardButton[][] getInlineKeyboardButtons(InlineKeyboardButton... buttonsArray) {
        List<List<InlineKeyboardButton>> buttons = new ArrayList<>();
        int rowsAmount = 2;
        int columnSize = (int) Math.ceil(buttonsArray.length / 2.0);
        for (int i = 0; i < rowsAmount; i++) {
            List<InlineKeyboardButton> tempButtonsArray = new ArrayList<>();
            for (int j = 0; j < columnSize; j++) {
                int index = j + (i * columnSize);
                if (buttonsArray.length != index) {
                    tempButtonsArray.add(buttonsArray[index]);
                }
            }
            buttons.add(tempButtonsArray);
        }

        return  buttons.stream()
                .map(arr -> arr.toArray(InlineKeyboardButton[]::new))
                .toArray(InlineKeyboardButton[][]::new);
    }


    /**
     * Create Inline Button
     * @param title button text
     * @param callBackData data for callback
     * @return InlineKeyboardButton
     */
    private InlineKeyboardButton buildInlineButton(String title, String callBackData) {
        return new InlineKeyboardButton(title).callbackData(callBackData);
    }
}
