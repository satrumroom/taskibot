package com.ua.satrumroom.bot.telegram;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.*;
import com.pengrad.telegrambot.request.BaseRequest;
import com.pengrad.telegrambot.request.SendMessage;
import com.ua.satrumroom.bot.Bot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TelegramBotService implements Bot {

    final TelegramBot telegramBot;

    public TelegramBotService(@Autowired TelegramBot telegramBot) {
        this.telegramBot = telegramBot;
        this.start();
    }

    public void start() {
        telegramBot.setUpdatesListener(updates -> {
            updates.forEach(this::processData);
            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        });
    }

    @Override
    public void sendMessage(long chatId, String message) {
        executeRequest(new SendMessage(chatId, message));
    }


    private void executeRequest(BaseRequest br) {
        telegramBot.execute(br);
    }


    public void processData(Update update) {
        Message message = update.message();
        CallbackQuery callbackQuery = update.callbackQuery();
        InlineQuery inlineQuery = update.inlineQuery();
        if (message != null) {
            if (message.text().equals(START_COMMAND)) {
                executeRequest(new TelegramInterfaceMaker().createMain(message));
            } else {
                executeRequest(new SendMessage(message.chat().id(), "Мне нечего на это ответить..."));
            }
        }

        if (callbackQuery != null) {
            processCallbackQuery(callbackQuery);
        }

        if (inlineQuery != null) {
            processInlineQuery(inlineQuery);
        }
    }

    private void processCallbackQuery(CallbackQuery callbackQuery) {
        if (callbackQuery.data().startsWith(SHOW_ALL_TASKS_COMMAND)) {
            executeRequest(new TelegramInterfaceMaker().showAllTasks(callbackQuery));
        }
    }

    private void processInlineQuery(InlineQuery inlineQuery) {
        // someday here will be code
    }
}
